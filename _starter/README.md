#Installing Gulp (and its submodules)

After installing Ruby and Node.js, type the following comands in `cmd`: 

`npm install`

You'll need some globally installed plugins, as Styledocco

`npm install styledocco -g`

You'll also need some ruby gems installed

`gem install compass`
`gem install scss-lint`


Now go to your project root and type:

`gulp`
