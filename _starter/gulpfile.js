//load plugins
var gulp           = require('gulp'),
	compass          = require('gulp-compass'),
	postcss          = require('gulp-postcss'),
	autoprefixer     = require('gulp-autoprefixer'),
	minifycss        = require('gulp-minify-css'),
	uglify           = require('gulp-uglify'),
	rename           = require('gulp-rename'),
	concat           = require('gulp-concat'),
	notify           = require('gulp-notify'),
	livereload       = require('gulp-livereload'),
	plumber          = require('gulp-plumber'),
	path             = require('path'),
  csslint          = require('gulp-csslint'),
	scsslint 		     = require('gulp-scss-lint'),
  styledocco       = require('gulp-styledocco'),
  concat           = require('gulp-concat'),
  eslint           = require('gulp-eslint'),
	webserver		     = require('gulp-webserver');
  
	
//the title and icon that will be used for the Grunt notifications
var notifyInfo = {
	title: 'Gulp',
	icon: path.join(__dirname, 'gulp.png')
};

//error notification settings for plumber
var plumberErrorHandler = { errorHandler: notify.onError({
		title: notifyInfo.title,
		icon: notifyInfo.icon,
		message: "Error: <%= error.message %>"
	})
};

// if you need a webserver for your html / css / js...
gulp.task('webserver', function() {
  return gulp.src('./public')
    .pipe(webserver({
      livereload: true,
	  host: '0.0.0.0',
      open: 'http://localhost:1337/index.html',
	  port: 1337,
    directoryListing: {
		enable: true,
        path:   './public',
		    fallback: 'index.html'  
	  }	  
    }));
});

// Compiling SCSS using SASS/Compass
gulp.task('styles', function() { 
	return gulp.src(['public/scss/**/*.scss'])
		.pipe(plumber(plumberErrorHandler))
		.pipe(compass({
			config_file: 'config.rb',
			css: 'public/css',
			sass: 'public/scss',
			image: 'public/img'
		}))
		.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
		.pipe(gulp.dest('public/css'))
		.pipe(rename({ suffix: '.min' }))
		.pipe(minifycss())
		.pipe(gulp.dest('public/css'));
});

// CSS Lint: will hurt your feelings (and help you code better)
gulp.task('css-lint', function() {
  gulp.src(['public/css/**/*.css', '!public/css/**/*.min.css'])
  .pipe(csslint('csslintrc.json'))
  .pipe(csslint.reporter());
});


// Linting our SCSS, to follow our style guide
gulp.task('scss-lint', function() {
  return gulp.src(['public/scss/**/*.scss', '!**/*/_normalize.scss', '!**/*/_helpers.scss'])
    .pipe(scsslint({
		'config': '_scss-lint.yml'
	}));
});

// concatenating CSS file do use in docs
gulp.task('docs-css', ['styles'], function () {
  return gulp.src('public/css/**/*.css')
    .pipe(concat("css/bundle.css"))
    .pipe(gulp.dest('docs/'));
});

// generating CSS docs
// @todo: corrigir o bug de pq não respeita a função  gulp.src(['public/scss/*.scss', '!public/scss/*/*.scss'])
gulp.task('styledocco', ['docs-css'], function () {
  return gulp.src(['public/scss/*.scss', '!public/scss/_helpers/*.scss'])
    .pipe(styledocco({
      out: 'docs',
      name: 'Front-end-Standards',
      include: ['docs/css/bundle.css'],
      'no-minify': true      
    }));
});

//watch for changes in docs
gulp.task('watch-docs', ['styledocco'], function() {
	//watch .scss files
	gulp.watch('public/scss/*.scss', ['styledocco']);
});

// serving docs
gulp.task('docs', ['watch-docs'], function() {
  return gulp.src('./docs')
    .pipe(webserver({
      livereload: true,
	    host: '0.0.0.0',
      open: 'http://localhost:1415/index.html',
      port: 1415,
      directoryListing: {
      enable: true,
          path:   './docs',
          fallback: 'index.html'  
      }	  
    }));
});

gulp.task('js-lint', function () {
    return gulp.src(['public/js/**/*.js', '!public/js/vendor/**/*.js'])
        // eslint() attaches the lint output to the eslint property
        // of the file object so it can be used by other modules.
        .pipe(eslint('.eslintrc'))
        // eslint.format() outputs the lint results to the console.
        // Alternatively use eslint.formatEach() (see Docs).
        .pipe(eslint.format())
        // To have the process exit with an error code (1) on
        // lint error, return the stream and pipe to failAfterError last.
        .pipe(eslint.failAfterError());
});

//watch for css changes
gulp.task('watch-styles', function() {
	//watch .scss files
	gulp.watch('public/scss/**/*.scss', ['styles', 'scss-lint', 'css-lint']);
});

gulp.task('watch-scripts', function() {
	//watch .scss files
	gulp.watch('public/scss/**/*.scss', ['scripts', 'js-lint']);
});


gulp.task('default', ['styles','scss-lint','css-lint','watch-styles', /*,'webserver'*/]); // webserver should only be used when you want to have a local webserver for html/css/js files

gulp.task('jenkins', ['styles']);