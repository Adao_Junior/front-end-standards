# Code Review

> Itelios code review best practices.

## TOC

1. [Pull requests](#pull-requests)
2. [Reviewing](#reviewing)
3. [Shipping](#shipping)
4. [Common sense](#common-sense)

## Pull requests

* [1.1](#1.1) <a name='1.1'></a> Always before opening a pull request make sure you run CSS and JavaScript linting.

> This will avoid micro reviews based on code syntax.

* [1.2](#1.2) <a name='1.2'></a> Everytime you open a pull request make sure to let your reviewer know. *Ping him/her on Skype!* 

Also, when you develop a new feature, you'll probably want the whole front-end team to take a look at it and make their comments.

> Use our `ML - Front End` Skype chat room or even `ML - Front End Global`.

* [1.3](#1.3) <a name='1.3'></a> Always before opening a pull request make sure the commit history is clean and easy to understand.

**[⬆ back to top](#toc)**

## Reviewing

* [2.1](#2.1) <a name='2.1'></a> When reviewing code, make sure you add coments to specific line numbers.

> Bitbucket lets us do that quick and easy.

**[⬆ back to top](#toc)**

## Shipping

* [3.1](#3.1) <a name='3.1'></a> After merging a branch, make sure you hit <kbd>Delete branch</kbd> button on Bitbucket's interface.

> It will help keeping things clean.

**[⬆ back to top](#toc)**

## Common sense

* [4.1](#4.1) <a name='4.1'></a> Use your common sense when reviewing code. Avoid pointless discussions, [bikeshedding](http://en.wikipedia.org/wiki/Parkinson%27s_law_of_triviality) and drama.

> This is probably the hardest one. Just try to be professional and kind.

**[⬆ back to top](#toc)**
