# CSS

> Itelios CSS Best Practices.

## TOC

1. [Responsive Web](#responsive-web)
2. [Grid System](#grid-system)

## Responsive Web

* [1.1](#1.1) <a name='1.1'></a> The web is responsive (and so should be our code). Avoid at all costs using absolute widths (px), try to use percentage instead.

* [1.2](#1.2) <a name='1.2'></a> The easier, most readable way to use media queries is to open it inside the component it belongs to. We can use variable parsing to use the `$mobile` variable we've defined in our [_variables.scss](/_starter/public/scss/_helpers/_variable.scss). 

```
.slider {
  width:100%;
  @media #{$mobile} {
      width:90%;
      margin:0 auto;
  } 
}
```

**[⬆ back to top](#toc)**

## Grid System

* [2.1](#2.1) <a name='2.1'></a> Use a grid system!
  1. It helps us be sure our layouts have visual integrity, with default spacings and columns sizes.
  2. It makes our CSS cleaner without using a s**tload of floats and widths. 

**[⬆ back to top](#toc)**