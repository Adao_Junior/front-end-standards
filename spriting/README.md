# CSS Sprites

> Itelios spriting Standards.

## TOC

1. [Basic Usage](#basic-usage)
2. [Base64 Usage](#syntax)

## Basic Usage

* [1.1](#1.1) <a name='1.1'></a> We use [Compass](http://compass-style.org/) to render and implement our PNG sprites.

> Right now we're using a [retina-sprite mixin](/_starter/public/scss/_helpers/_retina-sprites.scss) in order to implement retina images where they're needed.

* [1.2](#1.2) <a name='1.2'></a> Simply add the icon you want to add in the sprite to [img/ico](/_starter/public/img/ico/) and the image with double the size (retina) to [img/ico-retina](/_starter/public/img/ico-retina/)

* [1.3](#1.3) <a name='1.3'></a> Add the icon (preferrably to pseudo-elements) using `@include ico(name-of-the-icon)`. 

**[⬆ back to top](#toc)**

## Base64 embedding

* [2.1](#2.1) <a name='2.1'></a> If by any reason you'll need the sprite to be added as base64 (if you have cross-domain issues, for instance) you can call it in a slightly different way: `@include ico(name-of-the-icon, $base64: true)`.  

**[⬆ back to top](#toc)**
