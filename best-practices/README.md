# Best Practices

> Itelios general development best practices.

## TOC

1. [EditorConfig](#editorconfig)

## EditorConfig

* Make sure to use the same [`.editorconfig`](/_starter/.editorconfig) file accross all Itelios projects.

> [EditorConfig](http://editorconfig.org) help us maintain consistent coding styles between different editors and IDEs.

**[⬆ back to top](#toc)**
