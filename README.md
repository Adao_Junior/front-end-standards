## Front End Standards

> Guidelines for shipping great code @ Itelios :)

## Goal

> It’s harder to read code than to write it. - Joel Spolsky

The goal of this repository is to document the standards we use to write and publish code at [Itelios](http://itelios.com).

By following a common approach we ensure that the code will not have multiple identities, but a single one. This helps us maintain what we work on, onboard new people and focus on providing value.

All the standards proposed here are based on our own experience writing code and the many conversations we have in our weekly meetings. There's no such thing as right or wrong, only what works better for us and improves our workflow.

## Topics

* [General Best Practices](best-practices/README.md)
* [Markup Styleguide](markup/README.md)
* [CSS/SCSS Styleguide](styleguide/README.md)
* [CSS Best Practices](css/README.md)
* [CSS Spriting](spriting/README.md)
* [Javascript Styleguide / Best Practices](javascript-styleguide/README.md)
* [Javascript Default Plugins/Snippets](javascript-plugins/README.md)
* [Code Review](code-review/README.md)


## Contributing

If you have something you want to add, remove or improve, feel free to [open an issue](https://github.com/iteliosbrasil/front-end-standards/issues/new) and ping the team (at Skype: ML Front-end Global).

## Credits

Created and maintened by [a whole bunch of great people](https://github.com/iteliosbrasil/front-end-standards/graphs/contributors).

![Itelios](./itelios.png)

## License

[![CC0](http://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](http://creativecommons.org/publicdomain/zero/1.0)
