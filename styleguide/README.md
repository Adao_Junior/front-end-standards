# Styleguide

> Itelios CSS/SCSS code styleguide.

## TOC

1. [Preprocessor](#preprocessor)
2. [Syntax](#syntax)
3. [Best practices](#best-practices)
4. [Colors](#colors)
5. [Numbers and units](#numbers-and-units)
6. [Inline assets](#inline-assets)
7. [Pseudo elements](#pseudo-elements)
8. [Specificity and nesting](#specificity-and-nesting)
9. [Quotes](#quotes)
11. [Naming conventions](#naming-conventions)
12. [Namespaces](#namespaces)
13. [Whitespace](#whitespace)
14. [Code linting](#code-linting)
15. [Resources](#resources)

## Preprocessor

* [1.1](#1.1) <a name='1.1'></a> [Sass](http://sass-lang.com) + [Compass](http://compass-style.org/) is our preprocessor of choice.

* [1.2](#1.2) <a name='1.2'></a> `@extend` is allowed only when used with ghost classes/placeholders (%).

> `@extend`ing normal classes can lead to terrible code output - don't do that!

**[⬆ back to top](#toc)**

## Syntax

* [2.1](#2.1) <a name='2.1'></a> Use the Sassy CSS (SCSS) syntax.

> Since regular CSS code is valid SCSS, it's a win-win.

**[⬆ back to top](#toc)**

## Best practices

* [3.1](#3.1) <a name='3.1'></a> Avoid the use of `!important` at all costs. Exceptions to the rule:
	1. It's being used within a helper class;
	2. You can explain its use.

* [3.2](#3.2) <a name='3.2'></a> Avoid the use of ids at all costs.
> They kill modularity and are not necessary for styling.

* [3.3](#3.3) <a name='3.3'></a> When defining a variable, make sure it has a default value by using `!default` provided by Sass.
> That way overriding variables is safer.

* [3.4](#3.4) <a name='3.4'></a> Do not manually add vendor prefixes. 
> Our tooling should be able to handle this.

* [3.5](#3.5) <a name='3.5'></a> Do not use vendor-specific font rendering techniques.

> They are not consistent, break constrast and typography rules.

Use the general [font-face mixin](/_starter/public/scss/_helpers/_font-face.scss) instead.

* [3.6](#3.6) <a name='3.6'></a> Prefer `background` over `background-color` when possible.
> Simply because it's a shorthand.

```scss
// Bad
.selector {
  width: 480px;
}

// Good (values actually make sense)
.selector {
  width: 50%;
}
```

* [3.8](#3.8) <a name='3.8'></a> Avoid undoing styles.
> These are code smells and almost always have room for improvement.

```scss
// Bad
h2 {
  font-size: 2em;
  margin-bottom: 0.5em;
  padding-bottom: 0.5em;
  border-bottom: 1px solid #ccc;
}

.no-border {

  // Brute force resets down here
  padding-bottom: 0;
  border-bottom: none;
}

// Good
h2 {
  font-size: 2em;
  margin-bottom: 0.5em;
}

.headline {
  padding-bottom: 0.5em;
  border-bottom: 1px solid #ccc;
}
```

* [3.9](#3.9) <a name='3.9'></a> Prefer `em` over `px`.
> This allows for a more flexible element sizing.

**[⬆ back to top](#toc)**

## Colors


* [4.1](#4.1) <a name='4.1'></a> Prefer shorthand notation.

```scss
// Bad
$color: #CCCCCC;

// Good
$color: #CCC;

// Bad
$color: #FF6600;

// Good
$color: #F60;
```

* [4.2](#4.2) <a name='4.2'></a> Do not use CSS color names.

> They're not consistently implemented on browsers.

```scss
// Bad
.error {
  background: red;
}

// Good
.error {
  background: #f00;
}
```

**[⬆ back to top](#toc)**

## Numbers and units

* [5.1](#5.1) <a name='5.1'></a> Avoid specifying units for zero values.

```scss
// Bad
.square {
  border: 0px;
}

// Good
.square {
  border: 0;
}
```

* [5.2](#5.2) <a name='5.2'></a> Do not use floating decimals.

> They do make code harder to read.

```scss
// Bad
.heading {
  margin-left: -.75px;
  padding: .25px;
  border: 2.px;
}

// Good
.heading {
  margin-left: -0.75px;
  padding: 0.25px;
  border: 2.0px;
}
```

* [5.3](#5.3) <a name='5.3'></a> Do not add units for `line-height` values.

> Not doing so will break vertical rythm.

```scss
// Bad
p {
  line-height: 1.5px;
}

// Good
p {

  // Equivalent to 150% of the font size
  line-height: 1.5;
}
```

**[⬆ back to top](#toc)**

## Inline assets

* [6.1](#6.1) <a name='6.1'></a> Inline assets are only allowed if they weight less than or equal to `1KB` - or if you have some cross-domain issues - and are presented only once in the code.

**[⬆ back to top](#toc)**

## Pseudo elements

* [7.1](#7.1) <a name='7.1'></a> Use single collons `:` to access pseudo elements.

```scss
// Bad
.button::after {
  background: fuchsia;
}

// Good
.button:before {
  outline: 1px solid;
}
```

**[⬆ back to top](#toc)**

## Specificity and nesting

* [8.1](#8.1) <a name='8.1'></a> Avoid overly-specific selectors by making use of good ol' classes.
* [8.2](#8.2) <a name='8.2'></a> Maximum of `4` levels of nesting.

```scss
// Bad
.menu {
  background: orange;
  .menu-item {
    font-family: sans-serif;
    .link {
      &:hover {
       color: red;
      }
      > .link-label {
        font-weight: bold;
      }
    }
  }
}

// Good
.menu {
  background: orange;
}

.menu-item {
  font-family: sans-serif;
}

.link:hover {
  color: red;
}

.link-label {
  font-weight: bold;
}
```

**[⬆ back to top](#toc)**

## Quotes

* [9.1](#9.1) <a name='9.1'></a> Use single quotes `'` for everything.

* [9.2](#9.2) <a name='9.2'></a> Always wrap values with quotes.

> Even though some are not mandatory, it will enforce consistency.

```scss
// Bad
@import helpers/clearfix;

input[type=radio] {
  opacity: 0.35;
}

.selector {
  background: url(path/to/image.png) no-repeat;
}

// Good
@import 'helpers/clearfix';

input[type='radio'] {
  opacity: 0.35;
}

.selector {
  background: url('path/to/image.png') no-repeat;
}
```

**[⬆ back to top](#toc)**

## Naming conventions

* [11.1](#11.1) <a name='11.1'></a> Use `hyphen-case` to name classes, variables, functions, mixins and placeholders.

```scss
// Bad
.fooBar {
  border: none;
}

@function LoremIpsumDolor {
  text-align: center;
}

// Good
.foo-bar {
  outline: 1px solid red;
}


// Good
.lorem-ipsum-dolor {
  vertical-align: middle;
}
```

* [11.2](#11.2) <a name='11.2'></a> States can be prefixed with `is`, `has` or `should`.

```scss
// Bad
.logo.logoHidden {
  opacity: 0;
}

// Good
.logo.is-hidden {
  opacity: 0;
}

// Bad
.logo.logo-disabled {
  border: 1px solid;
}

// Good
.logo.is-disabled {
  border: 1px solid fuchsia;
}

// Bad
.button-with-icon {
  // ...
}

// Good
.button.has-icon {
  // ...
}
```

**[⬆ back to top](#toc)**

## Namespaces

> Namespaces help us understand what role classes play.

| Prefix | Description | Example |
|---|---|---|
| `c` | For user interface components | `c-dropdown` |
| `ab` | For A/B testing stuff (usually removed and re-implemented once the test is done) | `ab-jira-ticket-42` |
| `u` | For utils | `u-clearfix` |
| `t` | For custom themes | `t-black-friday` |
| `_` | For hacks (that should be removed as soon as possible) | `_fix-dropdown-ie8` |
| `is`, `has`, `should` | For component states | `is-disabled` |
| `v` | For vendor service hooks (such as Optimizely, Crazy Egg, etc) | `v-optimizely` |

**[⬆ back to top](#toc)**

## Whitespace

* [13.1](#13.1) <a name='13.1'></a> Add a space after selector definition.

```scss
// Bad
.selector{content: 'foo';}

// Good
.selector {
  content: 'foo'; 
}
```

* [13.2](#13.2) <a name='13.2'></a> Break all the rules into new lines.

> This will improve code readability.

```scss
// Bad
.section { cursor: pointer; text-align: center; }

// Good
.section {
  text-align: left;
  vertical-align: middle;
}
```

* [13.3](#13.3) <a name='13.3'></a> When targeting multiple selectors break each one in a new line.

```scss
// Bad
.footer, .header, .main {
  display: block;
}

// Good
.footer,
.header,
.main {
  margin: 0 auto;
}
```

* [13.4](#13.4) <a name='13.4'></a> Keep multiple rules in more than one line for better readability.
```scss
// Bad
.box {
  box-shadow: 0 1px 1px #eee,
  inset 0 1px 0 #f00;
}

// Good
.box {
  box-shadow: 0 1px 1px #eee, 
              0 1px 0   #f00 inset;
  background: linear-gradient(
              #1e5799 0%,
              #2989d8 50%,
              #207cca 51%,
              #7db9e8 100%);
}
```

**[⬆ back to top](#toc)**

## Organization

* [14.1](#14.1) <a name='14.1'></a> The order of rules declaration should look like the following.
	1. `@extend`
	2. `@import`
	3. Variable definitions
	4. Functions, placeholders and mixins
	5. Other rules
	
```scss
// Bad
.square {
  @include border-radius(3px);
  @extend %shape-base;
  @include box-sizing(border-box);
  @extend %square-base;
  // Other rules
}

// Good
.square {
  @extend %shape-base;
  @extend %square-base;
  @include border-radius(3px);
  @include box-sizing(border-box);

  // Other rules
}
```

**[⬆ back to top](#toc)**

## Code linting

* [15.1](#15.1) <a name='15.1'></a> We use [SCSS-Lint](http://stylelint.io) to lint our SCSS code. All the rules can be found on the (`_scss-lint.yml`)[/_starter/_scss-lint.yml] file.

**[⬆ back to top](#toc)**

## Resources

### CSS naming methodologies

* [SMACSS](https://smacss.com)
* [BEM](https://bem.info)
* [SUIT CSS](http://suitcss.github.io)

### Blog posts

* [Code smells in CSS](http://csswizardry.com/2012/11/code-smells-in-css)
* [More Transparent UI Code with Namespaces](http://csswizardry.com/2015/03/more-transparent-ui-code-with-namespaces)
* [Line-height units](http://tzi.fr/css/text/line-height-units#Unitless)
* [Medium’s CSS is actually pretty f***ing good](https://medium.com/@fat/mediums-css-is-actually-pretty-fucking-good-b8e2a6c78b06)
* [Why Ems?](https://css-tricks.com/why-ems)

**[⬆ back to top](#toc)**
