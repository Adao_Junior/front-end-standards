# JavaScript Plugins

> Itelios JavaScript Plugin defaults.

# TOC

1. [Sliders and carousels](#sliders-and-carousels)
2. [Mobile Verification](#mobile-verification)

## Sliders and carousels

[Slick Slider](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar#Automatic_semicolon_insertion) as his creator claims, is probably the last slider we'll ever need. 

It supports:

* Carousel (multiple items per view) or slider (one item per view)
* Fully responsive. Scales with its container.
* Separate settings per breakpoint (if needed)
* Uses CSS3 when available. Fully functional when not.
* Swipe enabled. Or disabled, if you prefer.
* Desktop mouse dragging
* Infinite looping.
* Fully accessible with arrow key navigation
* Add, remove, filter & unfilter slides
* Autoplay, dots, arrows, callbacks, etc...
* Destroyable :P

**[⬆ back to top](#toc)**

## Mobile Verification

Right now, we're using [MODETECT](/_starter/js/vendor/devicetypedetection.js) to detect mobile devices. You can use that to add a class to your html and target mobile devices (for things that can't rely on sizes).

**[⬆ back to top](#toc)**

